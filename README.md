# Swift template project

A template project for Swift

## Pre requisites

- [SwiftLint](https://github.com/realm/SwiftLint) - An experimental tool to enforce Swift style and conventions.
- [CocoaPods](https://cocoapods.org) - Dependency manager for Swift and Objective-C Cocoa projects.

## Requirements

- iOS 8.0
- Xcode 6.4
- Swift 1.2

## Todo

- [x] Remove Storyboard 
- [x] Add SwiftLint
- [x] Add CocoaPods
- [ ] Improve folder structure
- [ ] Transform project in Xcode template project